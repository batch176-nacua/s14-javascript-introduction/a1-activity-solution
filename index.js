console.log("Hello World")

let firstName = ''
let lastName = ''
let age = 0
let hobbies = ['Biking', 'Mountain Climbing', 'Swimming']
let workAddress = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}

function printUserInfo(firstName, lastName, age){
	console.log(`First Name: ${firstName} 
Last Name: ${lastName} 
Age: ${age}`)
	console.log(`Hobbies: `)
	console.log(hobbies)
	console.log("Work Address: ")
	console.log(workAddress)

	console.log(`${firstName} ${lastName} is ${age} years of age.`)
	console.log(`This was printed inside of the function`)
	console.log(hobbies)
	console.log(`This was printed inside of the function`)
	console.log(workAddress)

}
printUserInfo('John', 'Smith', 30)

function returnFunction(){
	return true;
}

let isMarried = returnFunction();
console.log(`The value of isMarried is ${isMarried}`)
console.log(`---PS: I was absent when the instruction was handed so I just followed the sample output and I might have some misunderstanding of the activity steps (especially #7). Thank you for your understanding :)`)